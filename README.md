# Conjugate Phase Retrieval

This repository contains MATLAB code for the numerical experiments associated to the manuscript ``Conjugate Phase Retrieval in Paley-Wiener Space'', authored by Chun-Kit Lai, Friedrich Littmann, and Eric Weber.