function [outputArg1] = altproj(inputArg1,inputArg2,inputArg3)
%inputArg1 = analysis matrix
%inputArg2 = vector from which to retrieve phase
%inputArg3 = number of iterations to perform
%inputArg4 = threshold to reach for consistent reconstruction (depricated)


synthesis = inv(inputArg1'*inputArg1)*inputArg1';

%v = abs(inputArg1*inputArg2);

%instances = 100;
%tempvec = zeros(3);
%err = 1;
%while err > inputArg4

v = inputArg2;

phase = exp(2*pi*i*rand(6,1));

w = zeros(3,inputArg3);
x = zeros(6,inputArg3);

x(:,1) = v.*phase;
w(:,1) = synthesis*x(:,1);

for n=2:inputArg3
    x(:,n) = ((inputArg1*w(:,n-1))./abs(inputArg1*w(:,n-1))).*v;
    w(:,n) = synthesis*x(:,n);
end

outputArg1 = w;
end



