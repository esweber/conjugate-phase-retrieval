function [outputArg1] = conjugate(inputArg1,inputArg2,inputArg3,inputArg4)
%inputArg1 = analysis matrix
%inputArg2 = vector from which to retrieve phase
%inputArg3 = number of iterations to perform
%inputArg4 = threshold to reach for consistent reconstruction


synthesis = inv(inputArg1'*inputArg1)*inputArg1';

%v = abs(inputArg1*inputArg2);

instances = 100;

tempvec = zeros(3);

err = 1;

%while err > inputArg4

for j=1:instances
    phase = exp(2*pi*i*rand(6,1));

    w = zeros(6,inputArg3);

    w(:,1) = inputArg2.*phase;

    for n=2:inputArg3
        w(:,n) = ((inputArg1*synthesis*w(:,n-1))./abs(inputArg1*synthesis*w(:,n-1))).*inputArg2;
    end
    
    temperr = norm(abs(inputArg1*synthesis*w(:,inputArg3)) - inputArg2);
    
    if err > temperr
        err = temperr;
        tempvec = synthesis*w(:,inputArg3);
    end
        
end

outputArg1 = tempvec;
end

