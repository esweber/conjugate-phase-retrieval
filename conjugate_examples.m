%
% This script is to generate 1,000 examples of the conjugate phase
% retrieval instances with reconstruction using the alternating
% projections.  We will determine the number of iterations needed to
% achieve a certain level of approximation error.
%
%

iterations = 900;


A = [1, 0, 0; 0, 1, 0; 0, 0, 1; 1, -1, 0; 1, 0, -1; 0, 1, -1];


synthesis = inv(A'*A)*A';

% This array will be 1,000 random 3d vectors.
u = rand(3,1000).*exp(2*pi*i*rand(3,1000));

v = abs(A*u);

phases = zeros(3,1000,iterations);

errors = ones(2,1000,iterations);

for m=1:1000
    phases(:,m,:) = altproj(A,v(:,m),iterations);
end

frou = zeros(3,3,1000);
frop = zeros(3,3,1000,iterations);
fropc = zeros(3,3,1000,iterations);

for n=1:1000
    for o=1:iterations
        frou(:,:,n) = u(:,n)*u(:,n)';
        frop(:,:,n,o) = phases(:,n,o)*phases(:,n,o)';
        fropc(:,:,n,o) = conj(phases(:,n,o))*conj(phases(:,n,o))';
        errors(1,n,o) = (norm(frou(:,:,n) - frop(:,:,n,o),'fro'))/norm(frou(:,:,n),'fro');
        errors(2,n,o) = (norm(frou(:,:,n) - fropc(:,:,n,o),'fro'))/norm(frou(:,:,n),'fro');
    end
end

errorsmin = squeeze(min(errors(1,:,:),errors(2,:,:)));
errors1 = double(errorsmin>10^-8);
errors2 = sum(errors1,2);
errors3 = double(errors1(:,900)==0);
errors4 = errors2.*errors3;

sum(errors3)
mean(nonzeros(errors4))
median(nonzeros(errors4))



