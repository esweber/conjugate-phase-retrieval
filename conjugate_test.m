function [outputArg1] = conjugate_test(vec1,vec2,iter)
% This function rectifies the conjugate and phase between two vectors.
% The input vectors are 3x1 arrays, which consist of samples of the unknown
% funtion f.  The 1st and 2nd entries of vec1 should be the same as the 2nd
% and 3rd entries of vec2 after being rectified.

conjugate = 0;
phase = 0;


tvec1 = [vec1(1) ; vec1(2) ];
tvec1 = tvec1/norm(tvec1);

tvec2 = [vec2(2) ; vec2(3) ];
tvec2 = tvec2/norm(tvec2);

tnorm1 = norm(tvec1*tvec1' - tvec2*tvec2','fro');
tnorm2 = norm(tvec1*tvec1' - conj(tvec2)*conj(tvec2)','fro');

%det1 = vec1(1)*vec2(3) - vec1(2)*vec2(2);
%det2 = vec1(1)*conj(vec2(3)) - vec1(2)*conj(vec2(2));

%if min(tnorm1,tnorm2) > 1e-4
%    sprintf("these vectors are inconsistent: sample %d", iter)
%end

if tnorm1 <= tnorm2
    conjugate = 1;
%    phase = vec1(1)*abs(vec2(2))/(vec2(2)*abs(vec1(1)));
    phase = (vec1(1)/vec2(2))/abs(vec1(1)/vec2(2));
    tempvec = phase*vec2;
else
    conjugate = -1;
%    phase = vec1(1)*abs(vec2(2))/(conj(vec2(2))*abs(vec1(1)));
    phase = (vec1(1)/conj(vec2(2)))/abs(vec1(1)/conj(vec2(2)));
    tempvec = phase*conj(vec2);
end

%outputArg1 = [ det1 ; det2 ];

%outputArg1 = [ conjugate; phase ];

%vec1(1)*tempvec(3) - vec1(2)*tempvec(2)


outputArg1 = tempvec;



end

