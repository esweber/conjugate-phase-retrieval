% This code is to generate an example of conjugate phase retrieval in the
% Paley-Wiener space.
%
%

%function [outputArg1,outputArg2] = cpr_pws(vec1,vec2,iter)

% Generate the coefficients of a random, complex-valued, bandlimited
% signal.  Force a zero-crossing at 0.
%
coeff = rand(21,1).*exp(2*pi*i*rand(21,1));

coeff(11) = 0;

%coeffa = coeff;
%coeffa(11) = 0;

% Define the structured convolution matrix.
A = [1, 0, 0; 0, 1, 0; 0, 0, 1; 1, -1, 0; 1, 0, -1; 0, 1, -1];

% Choose parameters for the reconstruction of the samples of the convoluted
% signals.

error = 1e-4;
iterations = 900;
beta = rand;
%beta = 0;
%beta = 0.25;


% Define interpolation matrices
ta = linspace(-10,10,21);
tb1 = linspace(-21,21,85);
tb2 = linspace(-10,10,41);
tb3 = linspace(-20,20,81);
tb4 = linspace(-15,15,61);

SINCa = sincmatrix(ta,0,tb1,1);
SINCb = sincmatrix(ta,beta,tb1,1);
SINCaa = sincmatrix(ta,0,tb2,1);
SINCab = sincmatrix(ta,beta,tb2,1);
SINCcb = sincmatrix(ta,beta,tb4,1);
SINC2a = sincmatrix(tb2,0,tb2,2);
SINC2b = sincmatrix(tb3,beta,tb3,2);

SINCbb = sincmatrix(ta,beta,ta,1);

coeffb = SINCbb*coeff;




%Samps = zeros(3,81);
Sampsb = zeros(3,61);
Unphased = zeros(6,81);
Unphasedb = zeros(6,61);
%Dephased = zeros(3,81);
Dephasedb = zeros(3,61);
%DephasedFixed = zeros(3,81);
DephasedFixedb = zeros(3,61);



interp = SINCa*coeff;
interpb = SINCb*coeff;
interpbf = abs(interpb(4:84)-interpb(3:83));
interpbff = abs(interpb(4:84) - interpb(2:82));
tc = linspace(-20,20,161);



for k=1:81
    Samps(1,:) = interp(4:84);
    Samps(2,:) = interp(3:83);
    Samps(3,:) = interp(2:82);
end


Unphased = abs(A*Samps);

Fsqtemp = SINC2b*(Unphased(2,:).^2)';
Ffsqtemp = SINC2b*(Unphased(4,:).^2)';
Fffsqtemp = SINC2b*(Unphased(5,:).^2)';

Fsq = sqrt(Fsqtemp(9:73));
Ffsq = sqrt(Ffsqtemp(9:73));
Fffsq = sqrt(Fffsqtemp(9:73));


Unphasedb(1,:) = Fsq(4:64)';
Unphasedb(2,:) = Fsq(3:63)';
Unphasedb(3,:) = Fsq(2:62)';
Unphasedb(4,:) = Ffsq(3:63)';
Unphasedb(5,:) = Fffsq(3:63)';
Unphasedb(6,:) = Ffsq(2:62)';

Unphasedb = real(Unphasedb);

samps = size(Unphasedb,2);



for m=1:samps
%    Dephased(:,m) = conjugate(A,Unphased(:,m),iterations,error);
    Dephasedb(:,m) = conjugate(A,Unphasedb(:,m),iterations,error);
end


%DephasedFixed(:,1) = Dephased(:,1);
DephasedFixedb(:,1) = Dephasedb(:,1);

for n=2:samps
%    DephasedFixed(:,n) = conjugate_test(DephasedFixed(:,n-1),Dephased(:,n),n);
    DephasedFixedb(:,n) = conjugate_test(DephasedFixedb(:,n-1),Dephasedb(:,n),n);
end


%for k=1:21
%    frecon(k,1) = DephasedFixed(2,2*k+19);
%    freconb(k,1) = DephasedFixedb(2,2*k+19);
%end

freconb = DephasedFixedb(2,:).';

%finterp = SINCaa*coeff;
finterpb = SINCcb*coeff;


%norm1 = norm(coeff*coeff' - frecon*frecon','fro')/norm(coeff*coeff','fro');
%norm2 = norm(coeff*coeff' - conj(frecon)*conj(frecon)','fro')/norm(coeff*coeff','fro');
%if norm2<norm1
%    frecon = conj(frecon);
%end

norm3 = norm(finterpb*finterpb' - freconb*freconb','fro')/norm(finterpb*finterpb','fro');
norm4 = norm(finterpb*finterpb' - conj(freconb)*conj(freconb)','fro')/norm(finterpb*finterpb','fro');


if norm3>norm4
    freconb = conj(freconb);
end


%phaseraw = (sum(coeff(1:10,1)./frecon(1:10,1)) + sum(coeff(12:21,1)./frecon(12:21,1)))/20;

%phaseraw = mean(coeff./frecon);
%phase = phaseraw/abs(phaseraw);
%frecon = phase*frecon;

phaserawb = mean(finterpb./freconb);
phaseb = phaserawb/abs(phaserawb);
freconb = phaseb*freconb;


norm5 = norm(finterpb*finterpb' - freconb*freconb','fro')/norm(finterpb*finterpb','fro');

SINCddd = sincmatrix(tb4,0,tc,1);
finterpddd = 0.5*SINCddd*finterpb;
freconbddd = 0.5*SINCddd*freconb;

SINCdd = sincmatrix(ta,0,tc,1);
finterpdd = SINCdd*coeff;

figure(1);
plot(tc,real(finterpdd))
hold on
scatter(ta,real(coeff),'filled')
plot(tc,real(freconbddd))


figure(2);
plot(tc,imag(finterpdd))
hold on
scatter(ta,imag(coeff),'filled')
plot(tc,imag(freconbddd))


figure(3);
plot(tc,real(finterpdd))
hold on
scatter(ta,real(coeff),'filled')
plot(tc-beta,real(freconbddd))


figure(4);
plot(tc,imag(finterpdd))
hold on
scatter(ta,imag(coeff),'filled')
plot(tc-beta,imag(freconbddd))




