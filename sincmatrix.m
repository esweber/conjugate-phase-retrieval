function [outputArg1] = sincmatrix(inputArg1,inputArg2,inputArg3,inputArg4)
% This function generates a sinc matrix for interpolation of the function
% samples.
%   inputArg1 = points of original samples
%   inputArg2 = offset
%   inputArg3 = points to be interpolated 
%   inputArg4 = scale


ta=inputArg1;
tb=inputArg3;

outputArg1 = zeros(size(tb,2),size(ta,2));

for k=1:size(tb,2)
    for m=1:size(ta,2)
        outputArg1(k,m) = sinc(inputArg4*(tb(k) - inputArg2 - ta(m)));
    end
end


%outputArg1 = SINC;

end

